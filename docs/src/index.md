```@meta
CurrentModule = Meander
DocTestSetup = quote
    using Meander
end
```

# Meander

Documentation for [Meander](https://gitlab.com/seamsay/Meander.jl).

!!! note
    
    The language used in this documentation can be somewhat clunky.
    This is because there are several concepts which would normally be referred to with the same or similar words.
    See the glossary on [`Meander`](@ref), and if you can come up with a better one then please let me know!

Meander allows you to define iterators that iterate over paths in some vector space.
Give it a list of the points which define the path, and tell it how many iterations you want, and you'll get back an iterator that iterates through that path:

```jldoctest home
julia> path = Path(
           10,
           ["Γ" => [0.0, 0.0], "M" => [1.0, 0.0], "K" => [1.0, sqrt(3) / 3], "Γ" => [0.0, 0.0]],
       )
Path{String, Vector{Float64}}:
   Γ: [0.0, 0.0] @ 1
=> M: [1.0, 0.0] @ 4
=> K: [1.0, 0.5773502691896257] @ 6
=> Γ: [0.0, 0.0] @ 10

julia> collect(path)
10-element Vector{Vector{Float64}}:
 [0.0, 0.0]
 [0.3333333333333333, 0.0]
 [0.6666666666666666, 0.0]
 [1.0, 0.0]
 [1.0, 0.28867513459481287]
 [1.0, 0.5773502691896257]
 [0.75, 0.4330127018922193]
 [0.5, 0.28867513459481287]
 [0.25, 0.14433756729740643]
 [0.0, 0.0]
```

You can use [`indices`](@ref), [`points`](@ref), and [`tags`](@ref) to retrieve information about the path:

```jldoctest home
julia> indices(path)
4-element view(::Vector{Int64}, :) with eltype Int64:
  1
  4
  6
 10

julia> points(path)
4-element view(::Vector{Vector{Float64}}, :) with eltype Vector{Float64}:
 [0.0, 0.0]
 [1.0, 0.0]
 [1.0, 0.5773502691896257]
 [0.0, 0.0]

julia> tags(path)
4-element view(::Vector{String}, :) with eltype String:
 "Γ"
 "M"
 "K"
 "Γ"
```

[`points`](@ref), and [`tags`](@ref) just repeat the information you already gave, but [`indices`](@ref) gives the information calculated by [`Path`](@ref).

## Table Of Contents

```@contents
Pages = ["example.md", "api.md"]
```

## API Index

```@index
```
