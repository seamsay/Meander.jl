using Meander
using Documenter

DocMeta.setdocmeta!(Meander, :DocTestSetup, :(using Meander); recursive = true)

makedocs(;
    modules = [Meander],
    authors = "Sean Marshallsay <srm.1708@gmail.com> and contributors",
    repo = "https://gitlab.com/seamsay/Meander.jl/blob/{commit}{path}#{line}",
    sitename = "Meander.jl",
    format = Documenter.HTML(;
        assets = String[],
        canonical = "https://seamsay.gitlab.io/Meander.jl",
        prettyurls = get(ENV, "CI", "false") == "true",
    ),
    pages = ["Home" => "index.md", "Examples" => "example.md", "API Reference" => "api.md"],
)
