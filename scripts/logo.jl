using Luxor

function circles()
    # TODO: Is there some concept of an anti-clip?
    #       Right now we have to draw the left-side of the path, and a box covering that side, and draw the circles with that as a clip area.
    #       Then we have to do the same with the right side of the path and draw the circles a second time.
    #       It would be much easier if we could just draw the path and clip to everything _outside_ the path.

    drawbezierpath(
        makebezierpath([
            Point(0, 250),
            Point(-200, 100),
            Point(100, 0),
            Point(-100, -100),
            Point(100, -250),
        ]),
        :path,
        close = false,
    )
    line(Point(-250, -250))
    line(Point(-250, 250))
    line(Point(-50, 250))
    do_action(:clip)
    juliacircles(outercircleratio = 1.25)
    clipreset()

    drawbezierpath(
        makebezierpath([
            Point(200, 250),
            Point(-50, 100),
            Point(200, 0),
            Point(-50, -100),
            Point(100, -250),
        ]),
        :path,
        close = false,
    )
    line(Point(250, -250))
    line(Point(250, 250))
    line(Point(50, 250))
    do_action(:clip)
    juliacircles(outercircleratio = 1.25)
    clipreset()
end

function text()
    oldsize = get_fontsize()
    fontsize(60)
    Luxor.text("Meander.jl", Point(0, 250), halign = :center)
    fontsize(oldsize)
end

function logo()
    background(0, 0, 0, 0)
    origin()
    circles()
end

const LOGO_PATH = joinpath(dirname(@__DIR__), "docs", "src", "assets", "logo.svg")

function draw(path = LOGO_PATH; title = false)
    mkpath(dirname(path))

    (_, ext) = splitext(path)
    type = if ext == ".svg"
        :svg
    elseif ext == ".png"
        :png
    else
        error("Unexpected file extension: $ext")
    end

    Drawing(600, 600, type, path)

    logo()
    if title
        text()
    end

    finish()
end

if abspath(PROGRAM_FILE) == @__FILE__
    draw()
    draw(joinpath(dirname(LOGO_PATH), "logo.png"))
    draw(joinpath(dirname(LOGO_PATH), "logo-title.svg"), title = true)
end
