{
  description =
    "A Julia package for easily defining and iterating through paths in abstract vector spaces.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-21.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = pkgs.mkShell { packages = with pkgs; [ julia-stable-bin ]; };
      });
}
